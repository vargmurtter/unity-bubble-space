﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreMenu : MonoBehaviour {

    [SerializeField]
    private Text scoresText;
    [SerializeField]
    private Menu _menuManager;

    private SaveManager _saveManager;

	void Start () {
        _saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();

        if (PlayerPrefs.HasKey("scores"))
        {
            string[] scores = _saveManager.LoadScore();
            int[] intScores = new int[scores.Length];
            for (int i = 0; i < scores.Length; i++)
            {
                intScores[i] = int.Parse(scores[i]);
            }

            Array.Sort(intScores);

            string total = "";
            for (int i = intScores.Length - 1; i > 0; i--)
            {
                total += intScores[i] + "\n";
            }

            scoresText.text = total;

        }

	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _menuManager.MainMenuButton();
        }
    }

}
