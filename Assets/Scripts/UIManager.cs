﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    [Header("Main")]
    public Sprite[] lives;
    public GameObject titleImageDisplay;
    public Image livesImageDisplay;
    public Text scoreTextDisplay;
    public int score;

    [Header("Powerups Icons")]
    public Image tripleShotIcon;
    public Image speedIcon;
    public Image shieldIcon;

    public void UpdateLives(int currentLives)
    {
        livesImageDisplay.sprite = lives[currentLives];
    }

    public void UpdateScore()
    {
        score += 10;
        scoreTextDisplay.text = "Score: " + score;
    }

    public void ShowTripleShotIcon()
    {
        tripleShotIcon.enabled = true;
    }

    public void HideTripleShotIcon()
    {
        tripleShotIcon.enabled = false;
    }

    public void ShowSpeedIcon()
    {
        speedIcon.enabled = true;
    }

    public void HideSpeedIcon()
    {
        speedIcon.enabled = false;
    }

    public void ShowShieldIcon()
    {
        shieldIcon.enabled = true;
    }

    public void HideShieldIcon()
    {
        shieldIcon.enabled = false;
    }

}
