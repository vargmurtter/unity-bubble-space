﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

    [SerializeField]
    private float _speed = 3f;
    [SerializeField]
    private GameObject _enemyExplosionPrefab;
    
    private UIManager _uiManager;

    private void Start()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
    }

    private void Update ()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);

        if (transform.position.y <= -6.5f)
        {
            float randomX = Random.Range(-6.5f, 6.5f);
            transform.position = new Vector3(randomX, 6.5f, 0);
        }
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Laser")
        {
            if (other.transform.parent != null)
            {
                Destroy(other.transform.parent.gameObject);
            }
            _uiManager.UpdateScore();
            Explosion();
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }else if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            if (player != null)
            {
                player.Damage();
            }
            _uiManager.UpdateScore();
            Explosion();
            Destroy(this.gameObject);
        }
    }

    private void Explosion()
    {
        if (_enemyExplosionPrefab != null)
        {
            GameObject explosion = Instantiate(_enemyExplosionPrefab, transform.position, Quaternion.identity);
            explosion.GetComponent<AudioSource>().Play();
            Destroy(explosion, 2.5f);
        }
    }
}
