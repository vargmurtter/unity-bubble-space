﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {
    
	void Start () {

        SpriteRenderer sr = GetComponent<SpriteRenderer>();

        if (sr == null)
            return;

        float width = sr.sprite.bounds.size.x;
        float height = sr.sprite.bounds.size.y;

        float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        Vector3 localScale = transform.localScale;

        localScale.x = worldScreenWidth / width;
        localScale.y = worldScreenHeight / height;

        transform.localScale = localScale;
        
    }
}
