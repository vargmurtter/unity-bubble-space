﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour {
    
    public void SaveScore(int score)
    {
        if (PlayerPrefs.HasKey("scores"))
        {
            string saves = PlayerPrefs.GetString("scores");
            Debug.Log("SAVES: " + saves);
            PlayerPrefs.SetString("scores", saves + "," + (score+10).ToString());
            PlayerPrefs.Save();
        }
        else
        {
            PlayerPrefs.SetString("scores", (score + 10).ToString());
            PlayerPrefs.Save();
        }
    }

    public string[] LoadScore()
    {
        string saves = PlayerPrefs.GetString("scores");
        return saves.Split(',');
    }

}
