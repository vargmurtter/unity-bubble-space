﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public bool canTripleShot = false;
    public bool isSpeedBoostActive = false;
    public bool isShieldsActive = false;

    public int lives = 3;

    [SerializeField]
    private float _speed = 5f;
    [SerializeField]
    private float _fireRate = 0.25f;
    private float _canFire = 0f;

    [SerializeField]
    private GameObject _laserPrefab;
    [SerializeField]
    private GameObject _tripleShotPrefab;
    [SerializeField]
    private GameObject _explosionPrefab;
    [SerializeField]
    private GameObject _shieldGameObject;
    [SerializeField]
    private GameObject[] _engines;
    
    private UIManager _uiManager;
    private GameManager _gameManager;
    private SaveManager _saveManager;
    private Menu _menuManager;
    private AudioSource _audioSource;

    private int hitCount;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        transform.position = new Vector3(0, -4f, 0);

        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        if (_uiManager != null)
        {
            _uiManager.UpdateLives(lives);
        }

        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
        _menuManager = GameObject.Find("Menu_Manager").GetComponent<Menu>();
        _audioSource = GetComponent<AudioSource>();

        hitCount = 0;
    }

    private void Update () {
        Movement();
        UserBounds();
        
        if (Input.GetMouseButton(0) || Input.GetKey(KeyCode.Space))
        {
            Shoot();
        }
	}

    private void Shoot()
    {
        if (Time.time > _canFire)
        {
            if (canTripleShot)
            {
                Instantiate(_tripleShotPrefab, transform.position, Quaternion.identity);
            }
            else
            {
                Instantiate(_laserPrefab, transform.position + Vector3.up, Quaternion.identity);
            }

            _audioSource.Play();

            _canFire = Time.time + _fireRate;
        }
    }

    private void UserBounds()
    {
        if (transform.position.y > 0)
        {
            transform.position = new Vector3(transform.position.x, 0, 0);
        }else if (transform.position.y < -4.2f)
        {
            transform.position = new Vector3(transform.position.x, -4.2f, 0);
        }

        if (transform.position.x > 7.2f)
        {
            transform.position = new Vector3(7.2f, transform.position.y, 0);
        }
        else if (transform.position.x < -7.2f)
        {
            transform.position = new Vector3(-7.2f, transform.position.y, 0);
        }

    }

    private void Movement()
    {
        float horizontal;
        float vertical;

        if (isSpeedBoostActive)
        {
            horizontal = Input.GetAxis("Horizontal") * _speed * 1.5f * Time.deltaTime;
            vertical = Input.GetAxis("Vertical") * _speed * 1.5f * Time.deltaTime;
        }
        else
        {
            horizontal = Input.GetAxis("Horizontal") * _speed * Time.deltaTime;
            vertical = Input.GetAxis("Vertical") * _speed * Time.deltaTime;
        }
        

        transform.Translate(new Vector3(horizontal, vertical, 0));
    }

    public void Damage()
    {

        if (isShieldsActive)
        {
            isShieldsActive = false;
            _uiManager.HideShieldIcon();
            _shieldGameObject.SetActive(false);
            return;
        }

        hitCount++;

        if (hitCount == 1)
        {
            _engines[0].SetActive(true);
        }
        else if (hitCount == 2)
        {
            _engines[1].SetActive(true);
        }

        lives--;
        _uiManager.UpdateLives(lives);
        if (lives < 1)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Explosion();
            _gameManager.gameOver = true;
            _saveManager.SaveScore(_uiManager.score);
            this.gameObject.SetActive(false);
            _menuManager.MainMenuButton();
        }
    }

    private void Explosion()
    {
        GameObject explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
        explosion.GetComponent<AudioSource>().Play();
        Destroy(explosion, 2.5f);
    }

    public void TripleShotPowerupOn()
    {
        canTripleShot = true;
        _uiManager.ShowTripleShotIcon();
        StartCoroutine(TripleShotPowerDownRoutine());
    }

    public void SpeedBoostPowerupOn()
    {
        isSpeedBoostActive = true;
        _uiManager.ShowSpeedIcon();
        StartCoroutine(SpeedBoostPowerDownRoutine());
    }

    public void EnableShields()
    {
        isShieldsActive = true;
        _uiManager.ShowShieldIcon();
        _shieldGameObject.SetActive(true);
    }

    IEnumerator TripleShotPowerDownRoutine()
    {
        yield return new WaitForSeconds(10f);
        canTripleShot = false;
        _uiManager.HideTripleShotIcon();
    }
    
    IEnumerator SpeedBoostPowerDownRoutine()
    {
        yield return new WaitForSeconds(5f);
        isSpeedBoostActive = false;
        _uiManager.HideSpeedIcon();
    }

    

}
