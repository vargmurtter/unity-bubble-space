﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    [SerializeField]
    private GameObject _enemyShipPrefab;
    [SerializeField]
    private GameObject[] _powerups;
    [SerializeField]
    private GameObject[] _particles;

    private GameManager _gameManager;

	private void Start () {

        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        StartCoroutine(EnemySpawnRoutine());
        StartCoroutine(PowerupsSpawnRoutine());
        StartCoroutine(ParticlesSpawnRoutine());
    }

    IEnumerator ParticlesSpawnRoutine()
    {
        while (true)
        {
            if (!_gameManager.gameOver)
            {
                float randomX = Random.Range(-8.5f, 8.5f);
                Instantiate(_particles[Random.Range(0,_particles.Length)], new Vector3(randomX, 6.5f, 0), Quaternion.identity);
            }
            yield return new WaitForSeconds(2f);
        }
    }

    IEnumerator EnemySpawnRoutine()
    {
        while (true)
        {
            if (!_gameManager.gameOver)
            {
                float randomX = Random.Range(-6.5f, 6.5f);
                Instantiate(_enemyShipPrefab, new Vector3(randomX, 6.5f, 0), Quaternion.identity);
            }
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator PowerupsSpawnRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(3f);
            float randomX = Random.Range(-6.5f, 6.5f);
            GameObject powerup = _powerups[Random.Range(0, _powerups.Length)];
            Instantiate(powerup, new Vector3(randomX, 6.5f, 0), Quaternion.identity);
        }
    }
}
