﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundParticles : MonoBehaviour {

    [SerializeField]
    private float _speed = 1f;

	private void Start ()
    {
        float axisScale = Random.Range(0.3f, 0.7f);
        transform.localScale = new Vector3(axisScale, axisScale, axisScale);
    }

    private void Update()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);

        if (transform.position.y <= -6.5f)
        {
            Destroy(this.gameObject);
        }

    }

}
