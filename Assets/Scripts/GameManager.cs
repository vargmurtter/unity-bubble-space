﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public bool gameOver = true;

    private UIManager _uiManager;

    private void Start()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        Instantiate(player, new Vector3(0, -4f, 0), Quaternion.identity);
        gameOver = false;
    }

    

}
