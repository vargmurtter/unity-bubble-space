﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    [SerializeField]
    private GameObject _transition;

    public void MainMenuButton()
    {
        StartCoroutine(TransitionRoutine("Menu"));
    }

	public void PlayButton()
    {
        StartCoroutine(TransitionRoutine("Game"));
    }

    public void ScoreButton()
    {
        StartCoroutine(TransitionRoutine("Scores"));
    }

    public void AboutButton()
    {
        StartCoroutine(TransitionRoutine("About"));
    }

    public void QuitButton()
    {
        Application.Quit();
    }

    private IEnumerator TransitionRoutine(string scene)
    {
        _transition.SetActive(true);
        Animator anim = _transition.GetComponent<Animator>();
        anim.SetBool("Clicked", true);
        yield return new WaitForSeconds(1.6f);
        SceneManager.LoadScene(scene);
    }

}
