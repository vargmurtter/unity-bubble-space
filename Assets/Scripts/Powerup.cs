﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour {

    [SerializeField]
    private float _speed = 3f;
    [SerializeField]
    private int powerupID; // 0 = triple shot, 1 = speed boost, 3 = shields
    [SerializeField]
    private AudioClip _clip;
	
	private void Update ()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);

        if (transform.position.y <= -6.5f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position);

            Player player = other.GetComponent<Player>();

            if (player != null)
            {
                if (powerupID == 0)
                {
                    player.TripleShotPowerupOn();
                } else if (powerupID == 1)
                {
                    player.SpeedBoostPowerupOn();
                } else if (powerupID == 2)
                {
                    player.EnableShields();
                }
            }

            Destroy(this.gameObject);
        }
    }
}
