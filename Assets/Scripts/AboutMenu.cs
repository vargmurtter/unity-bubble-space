﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AboutMenu : MonoBehaviour {

    [SerializeField]
    private Menu _menu;

	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _menu.MainMenuButton();
        }
	}
}
